# Maintainer: Patrycja Rosa <alpine@ptrcnull.me>
pkgname=py3-thefuzz
pkgver=0.21.0
pkgrel=0
pkgdesc="Fuzzy String Matching in Python"
url="https://github.com/seatgeek/thefuzz"
arch="noarch"
license="GPL-2.0-or-later"
depends="py3-levenshtein"
makedepends="py3-gpep517 py3-setuptools py3-wheel"
checkdepends="py3-pytest py3-hypothesis py3-pycodestyle"
subpackages="$pkgname-pyc"
source="https://github.com/seatgeek/thefuzz/archive/refs/tags/$pkgver/py3-thefuzz-$pkgver.tar.gz"
builddir="$srcdir/thefuzz-$pkgver"

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages .testenv
	.testenv/bin/python3 -m installer .dist/*.whl
	.testenv/bin/python3 -m pytest
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/*.whl
}

sha512sums="
60259185cc623bee667f9a04cdc878ba0aa1fdc2e7835cd2685a82d6960d77d4958dd19a709652815968266e0f0578467304c956b4c281409d86160d73e2f88a  py3-thefuzz-0.21.0.tar.gz
"
